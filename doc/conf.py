# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from datetime import date
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'RASCAS'
#copyright = '2024, Leo Michel-Dansac'
copyright = f'{date.today().year}, Leo Michel-Dansac'
author = 'Leo Michel-Dansac'

# The short X.Y version
version = '2.0'
# The full version, including alpha/beta/rc tags
release = ''


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
#extensions = [
#    'sphinx.ext.todo',
#    'sphinx.ext.coverage',
#    'sphinx.ext.imgmath',
#    'sphinx.ext.viewcode',
#]
extensions = [
    "sphinx.ext.mathjax",
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinx_design",
    "myst_nb",
    "IPython.sphinxext.ipython_console_highlighting",
]
#    "autoapi.extension",

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
#source_suffix = ['.rst', '.md']
source_suffix = ['.md']
#source_suffix = {'.md': 'markdown'}

# The master toctree document.
master_doc = 'index'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
#language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
#exclude_patterns = []
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "**.ipynb_checkpoints"]

# The name of the Pygments (syntax highlighting) style to use.
#pygments_style = None

nb_execution_mode = "off"

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_book_theme'
##html_theme = 'bizstyle'
#html_theme = 'sphinx_rtd_theme'
#html_theme = 'traditional'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
#primary_color = '#000000'
#primary_color_rgb_string = 'rgba(0, 0, 0, 1)' # For pdf, same color as `primary_color`
#secondary_color = '#FFD587'

#html_theme_options = {}
html_theme_options = {
    "use_repository_button": True,
   # "gitlab_url": "https://git-cral.univ-lyon1.fr/rascas/rascas",
#    "use_issues_button": True,
#    "use_edit_page_button": True,
#    "show_toc_level": 3,
}

html_logo = "./img/rascas_amr.png"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}


html_context = {
    "gitlab_url": "https://git-cral.univ-lyon1.fr", # or your self-hosted GitLab
    "gitlab_user": "rascas",
    "gitlab_repo": "rascas",
#    "edit_page_url_template": "{{ gitlab_url }}{{ gitlab_user }}{{ gitlab_repo }}",
}


# -- Options for simplePDF output ------------------------------------------------

simplepdf_vars = {
    #'primary': '#000000',
    #'links': '#FF3333',
    'primary': '#000000',
    'secondary': '#FFD587',
    'cover': '#ffffff',
    'white': '#ffffff',
    'links': 'FA2323',
    'cover-bg': '#1E90FF',
#    'cover-bg': 'rgba(0, 0, 0, 1)' ,
    'top-left-content': 'counter(page)',
}

# -- Extension configuration -------------------------------------------------

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
#todo_include_todos = True
