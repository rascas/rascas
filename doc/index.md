```{include} ../README.md
```

---

## Table of contents

```{toctree}
:caption: Getting Started
:maxdepth: 1

src/install.md
src/gitmodel.md
src/workflow.md
```

```{toctree}
:caption: Tutorials with Idealised Models
:maxdepth: 1

tutorials/IdealisedModels/tutorial_1/Tutorial-1.ipynb
tutorials/IdealisedModels/tutorial_2/Tutorial-2.ipynb
tutorials/IdealisedModels/tutorial_3/Tutorial-3.ipynb
tutorials/IdealisedModels/tutorial_4/Tutorial-4.ipynb
```

```{toctree}
:caption: Tutorials with Simulations
:maxdepth: 1

tutorials/RamsesSimulations/README.md
tutorials/RamsesSimulations/Halpha-From-Disk-Simulation/DiskSimulationMocks.ipynb
tutorials/RamsesSimulations/Halpha-From-Zoom-In-Simulation/Halpha-From-Zoom-In-Simulation.ipynb
```

```{toctree}
:caption: Parameters
:maxdepth: 1

src/params.md

```

```{toctree}
:caption: Developer Documentation
:maxdepth: 1

src/guidelines.md
src/contributors.md

```
