
# Acknowledgements

---

**Main developers**
- Leo Michel-Dansac
- Jeremy Blaizot



**Contributors and bug reporters**
- Thibault Garel
- Anne Verhamme
- Taysun Kimm
- Maxime Trebitsch
- Valentin Mauerhofer
- Joki Rosdahl
- Harley Katz
- Tiago Costa
- Cody Carr


