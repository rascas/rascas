# Guidelines for developers


RASCAS is an open source code. We welcome and encourage contributions to RASCAS.
Contributions could be in the form of bug reports, feature requests, and merge requests.
You can contact us using this email: rascas __@__ univ-lyon1.fr
For merge requests, just get an account to our [gitlab](https://git-cral.univ-lyon1.fr/rascas/rascas), and open a merge request. Remember to open a merge request to the develop branch. 

