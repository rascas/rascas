This directory contains a number of notebooks to learn how to use **RASCAS**. 

**RASCAS** can be used either to process RAMSES simulations or with idealised models (describing the gas and dust distribution and kinematics). 

The **RASCAS** suite works in three steps:
- Generate initial conditions for the sources of radiation.
- Create the mesh of the scattering medium, either by extracting from RAMSES outputs the gas (and dust) distribution in the computational volume, or by projecting the analytic model on an adaptively refined  mesh. 
- Actually compute the radiation transfer.

Each tutorial shows step-by-step how to prepare the parameter files, run the **RASCAS** suite, and visualize the results.
